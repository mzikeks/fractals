﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRFractals
{
    //Это класс главной формы.
    //Здесь будут создаваться фракталы и соответствующие им элементы управления (из папки FractalControls).
    //Элементы управления - UserControls - это просто части формы, которые можно добавлять на форму.
    //Внутри них нет никакой логики, все обработчики событий будут добавлены здесь или в классе Fractal (смотря где будет логичнее).

    //Каждый фрактал находится в папке FractalClasses и представляет собой обычный класс. Главный метод в нём - Draw.
    //Он будет вызываться из этого класса (так как он имеет досуп к PictureBox, в котором и будут рисоваться фракталы),
    //При помощи обработчика события FractalCanvas_Paint.
    public partial class Fractals : Form
    {
        private Fractal fractal;
        //Начальные значения размеров, чтобы знать на сколько нужно увеличить фрактал при изменении размеров окна.
        private int windowWidth;
        private int windowHeight;

        public Fractals()
        {
            //SetStyle(ControlStyles.ResizeRedraw, true);
            InitializeComponent();
            //Свойство формы, означающее, что все нажатия на клавиатуру будут обрабатываться сначала формой.
            this.KeyPreview = true;
            windowHeight = Height;
            windowWidth = Width;

        }

        //Обработчик события, вызванного изменением типа выбранного фрактала.
        private void FractalTypeChanger_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedState = FractalTypeChanger.SelectedItem.ToString();
            ControlsBox.Controls.Clear();
            if (selectedState == "Обдуваемое ветром фрактальное дерево")
            {
                fractal = new FractalTree(ControlsBox);
            }
            if (selectedState == "Кривая Коха")
            {
                fractal = new KochCurve(ControlsBox);
            }
            if (selectedState == "Ковер Серпинского")
            {
                fractal = new SierpinskySquare(ControlsBox);
            }
            if (selectedState == "Треугольник Серпинского")
            {
                fractal = new SierpinskyTriangle(ControlsBox);
            }
            if (selectedState == "Множество Кантора")
            {
                fractal = new CantorSet(ControlsBox);
            }
            //Если окно уже поменяло размер, нужно увеличить базовую длину отрезка.
            Fractals_Resize(this, e);
            fractal.generalControlsBox.SaveButton.Click += HandleSaveButton_Click;
            //Когда меняют параметры отрисовки вызывается событие ControlsChanged после которого нужно выполнить отрисовку.
            fractal.ControlsChanged += (sender, e) => FractalCanvas.Invalidate();
            FractalCanvas.Invalidate();
        }

        //Отрисовка фрактала, будет вызываться всегда когда это нужно программе (автоматически),
        //И при вызове Invalidate (при изменении каких-то параметров, в ручную).
        private void FractalCanvas_Paint(object sender, PaintEventArgs e)
        {
            if (fractal != null)
            {
                fractal.Draw(e);
            }
        }

        //Обработчик события нажатия кнопки для сохранения картинки.
        private void HandleSaveButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog savedialog = new SaveFileDialog
            {
                //Проверки на досупность указанного пути.
                OverwritePrompt = true,
                CheckPathExists = true,
                Filter = ".JPG|*.jpg|.PNG|*.png*",
            };

            if (savedialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Bitmap savedBit = new Bitmap(FractalCanvas.Width, FractalCanvas.Height);
                    FractalCanvas.DrawToBitmap(savedBit, FractalCanvas.ClientRectangle);
                    savedBit.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                catch
                {
                    MessageBox.Show("Невозможно сохранить изображение", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        //Обработчик события изменения размера окна.
        private void Fractals_Resize(object sender, EventArgs e)
        {
            if (fractal == null) return;
            fractal.firstLineCoef = Math.Min((double)Width / windowWidth, (double)Height / windowHeight);
            FractalCanvas.Invalidate();
        }

        //Обработчик события двойного клика по фракталу (зум).
        private void FractalCanvas_DoubleClick(object sender, EventArgs e)
        {
            if (fractal == null) return;
            fractal.NextZoom();
            FractalCanvas.Invalidate();
        }

        //Обработчик события движения фрактала (клавишами WASD).
        private void Fractals_KeyDown(object sender, KeyEventArgs e)
        {
            if (fractal == null) return;
            if (e.KeyCode == Keys.D)
            {
                fractal.MoveLeft();
            }
            if (e.KeyCode == Keys.A)
            {
                fractal.MoveRight();
            }
            if (e.KeyCode == Keys.W)
            {
                fractal.MoveDown();
            }
            if (e.KeyCode == Keys.S)
            {
                fractal.MoveUp();
            }
            FractalCanvas.Invalidate();
        }
    }
}