﻿using System;

namespace PRFractals
{
    partial class Fractals
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Fractals));
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.FractalCanvas = new System.Windows.Forms.PictureBox();
            this.FractalTypeChanger = new System.Windows.Forms.ComboBox();
            this.ControlsBox = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FractalCanvas)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(65, 122);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(250, 125);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.ControlsBox, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(941, 496);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.FractalCanvas, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.FractalTypeChanger, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(254, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(684, 490);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // FractalCanvas
            // 
            this.FractalCanvas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FractalCanvas.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("FractalCanvas.BackgroundImage")));
            this.FractalCanvas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FractalCanvas.InitialImage = null;
            this.FractalCanvas.Location = new System.Drawing.Point(3, 42);
            this.FractalCanvas.Name = "FractalCanvas";
            this.FractalCanvas.Size = new System.Drawing.Size(683, 448);
            this.FractalCanvas.TabIndex = 0;
            this.FractalCanvas.TabStop = false;
            this.FractalCanvas.Paint += new System.Windows.Forms.PaintEventHandler(this.FractalCanvas_Paint);
            this.FractalCanvas.DoubleClick += new System.EventHandler(this.FractalCanvas_DoubleClick);
            // 
            // FractalTypeChanger
            // 
            this.FractalTypeChanger.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FractalTypeChanger.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FractalTypeChanger.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FractalTypeChanger.FormattingEnabled = true;
            this.FractalTypeChanger.Items.AddRange(new object[] {
            "Обдуваемое ветром фрактальное дерево",
            "Кривая Коха",
            "Треугольник Серпинского",
            "Ковер Серпинского",
            "Множество Кантора"});
            this.FractalTypeChanger.Location = new System.Drawing.Point(75, 3);
            this.FractalTypeChanger.Margin = new System.Windows.Forms.Padding(75, 3, 75, 3);
            this.FractalTypeChanger.Name = "FractalTypeChanger";
            this.FractalTypeChanger.Size = new System.Drawing.Size(539, 33);
            this.FractalTypeChanger.TabIndex = 1;
            this.FractalTypeChanger.SelectedIndexChanged += new System.EventHandler(this.FractalTypeChanger_SelectedIndexChanged);
            // 
            // ControlsBox
            // 
            this.ControlsBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ControlsBox.AutoScroll = true;
            this.ControlsBox.ColumnCount = 1;
            this.ControlsBox.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ControlsBox.Location = new System.Drawing.Point(3, 3);
            this.ControlsBox.Name = "ControlsBox";
            this.ControlsBox.RowCount = 1;
            this.ControlsBox.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ControlsBox.Size = new System.Drawing.Size(245, 490);
            this.ControlsBox.TabIndex = 1;
            // 
            // Fractals
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(942, 493);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximumSize = new System.Drawing.Size(1920, 1080);
            this.MinimumSize = new System.Drawing.Size(960, 540);
            this.Name = "Fractals";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fractals";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Fractals_KeyDown);
            this.Resize += new System.EventHandler(this.Fractals_Resize);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FractalCanvas)).EndInit();
            this.ResumeLayout(false);

        }



        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.PictureBox FractalCanvas;
        private System.Windows.Forms.ComboBox FractalTypeChanger;
        private System.Windows.Forms.TableLayoutPanel ControlsBox;
    }
}

