﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PRFractals
{
    class SierpinskyTriangle: Fractal
    {
        public SierpinskyTriangle(TableLayoutPanel controlPanel)
            : base(controlPanel)
        {
            NumberIterations = 4;
            MaxNumberIterations = 6;
            FirstLineLength = 450;
        }

        //Метод для отрисовки "нулевого" шага рекурсии. Вызывется из главной формы при необходимости отрисовки.
        //Из него запустится рекурсивный алгоритм отрисовки фрактала.
        public override void Draw(PaintEventArgs fractalCanvas)
        {
            Graphics canvas = fractalCanvas.Graphics;
            canvas.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            canvas.Clear(Color.White);

            pen = new Pen(Color.Black, penThickness);
            pen.Color = GetCurrentColor(0);

            var pointA = new Point(fractalCanvas.ClipRectangle.Width / 2 - FirstLineLength / 2,
                                   fractalCanvas.ClipRectangle.Height - 15);
            var pointB = new Point(fractalCanvas.ClipRectangle.Width / 2 + FirstLineLength / 2,
                                   fractalCanvas.ClipRectangle.Height - 15);
            var pointC = new Point(fractalCanvas.ClipRectangle.Width / 2,
                                   fractalCanvas.ClipRectangle.Height - 15 - (int)(FirstLineLength * Math.Sqrt(3) / 2));
            
            pointA.Offset(horizontalOffset, verticalOffset);
            pointB.Offset(horizontalOffset, verticalOffset);
            pointC.Offset(horizontalOffset, verticalOffset);

            canvas.DrawPolygon(pen, new Point[]{ pointA, pointB, pointC});

            Draw(1, FirstLineLength, pointA, canvas);
        }

        //Метод, отвечающий за рекурсивную отрисовку фрактала.
        public void Draw(int numberIteration, int lineLength, Point pointA, Graphics canvas)
        {
            if (numberIteration > NumberIterations) return;

            var pointB = new Point(pointA.X + lineLength, pointA.Y);
            var pointC = new Point(pointA.X + lineLength / 2, pointA.Y - (int)(lineLength * Math.Sqrt(3) / 2));

            lineLength = (int)(lineLength / 2);
            pen.Color = GetCurrentColor(numberIteration);

            var newPointA = new Point((pointA.X + pointB.X) / 2, (pointA.Y + pointB.Y) / 2);
            var newPointB = new Point((pointB.X + pointC.X) / 2, (pointB.Y + pointC.Y) / 2);
            var newPointC = new Point((pointA.X + pointC.X) / 2, (pointA.Y + pointC.Y) / 2);

            canvas.DrawPolygon(pen, new Point[] { newPointA, newPointB, newPointC });

            Draw(numberIteration + 1, lineLength, pointA, canvas);
            Draw(numberIteration + 1, lineLength, newPointA, canvas);
            Draw(numberIteration + 1, lineLength, new Point(pointA.X + lineLength / 2, (pointC.Y + pointA.Y) / 2), canvas);
        }
    }
}
