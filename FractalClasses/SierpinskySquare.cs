﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PRFractals
{
    class SierpinskySquare: Fractal
    {   
        public SierpinskySquare(TableLayoutPanel controlPanel)
            : base(controlPanel)
        {
            NumberIterations = 3;
            MaxNumberIterations = 5;
            FirstLineLength = 400;
        }

        //Метод для отрисовки "нулевого" шага рекурсии вызывется из главной формы при необходимости отрисовки.
        //Из него запустится рекурсивный алгоритм отрисовки фрактала.
        public override void Draw(PaintEventArgs fractalCanvas)
        {
            Graphics canvas = fractalCanvas.Graphics;
            canvas.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            canvas.Clear(Color.White);

            pen = new Pen(Color.Black, penThickness);
            pen.Color = GetCurrentColor(0);
            SolidBrush brush = new SolidBrush(Color.Black);

            var rectangle = new Rectangle(fractalCanvas.ClipRectangle.Width / 2 - FirstLineLength / 2,
                                          (fractalCanvas.ClipRectangle.Height - FirstLineLength)/2,
                                          FirstLineLength, FirstLineLength);
            rectangle.Offset(horizontalOffset, verticalOffset);

            canvas.DrawRectangle(pen, rectangle);
            Draw(1, FirstLineLength, rectangle, brush, canvas);
        }

        //Рекурсивный метод отрисовки фрактала.
        public void Draw(int numberIteration, int lineLength, Rectangle rectangle, SolidBrush brush, Graphics canvas)
        {
            if (numberIteration > NumberIterations) return;

            lineLength = lineLength / 3;
            brush.Color = GetCurrentColor(numberIteration);
         
            var newRectangle = new Rectangle(rectangle.X + lineLength, rectangle.Y + lineLength, lineLength, lineLength);
            canvas.FillRectangle(brush, newRectangle);

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    newRectangle = new Rectangle(rectangle.X + lineLength * i, rectangle.Y + lineLength * j, 
                                                 lineLength, lineLength);
                    Draw(numberIteration + 1, lineLength, newRectangle, brush, canvas);
                }
            }
        }
    }
}
