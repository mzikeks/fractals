﻿using PRFractals.FractalClasses;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PRFractals
{
    //Класс, от которого наследуются все остальные фракталы.
    class Fractal
    {
        internal GeneralControls generalControlsBox;
        public event EventHandler ControlsChanged;
        internal double firstLineCoef = 1; //Меняется в зависимости от размера окна.
        private int[] zoomVariants = new int[] { 1, 2, 3, 5 };
        private int currentZoomIndex = 0;
        //Шаг движения "камеры" вокруг фрактала.
        private int offsetStep = 50;
        //Смещение "камеры" относительно фрактала.
        protected int horizontalOffset = 0;
        protected int verticalOffset = 0;

        protected bool useGradient = false;
        protected Gradient gradient;
        protected Pen pen;
        protected int penThickness = 2;
        protected Color startGradientColor = Color.FromArgb(0, 0, 0);
        protected Color endGradientColor = Color.FromArgb(0, 0, 0);

        private int recursionDepth;
        private int maxRecursionDepth;
        private int firstLineLength;
       
        //Будет вызываться из дочерних классов.
        //Создаем UserControl с TrackBar'ами изменения параметров и добавляем обрабочки событий для него.
        public Fractal(TableLayoutPanel controls)
        {
            this.generalControlsBox = new GeneralControls();
            generalControlsBox.RecursionDepthTrackBar.Scroll += HandleRecursionDepthScrolling;
            generalControlsBox.UseGradientCheckBox.CheckedChanged += HandleUseGradientCheckBoxChecked;

            generalControlsBox.StartGragientColorRedTrackBar.Scroll += HandleStartGradientColorChanged;
            generalControlsBox.StartGragientColorGreenTrackBar.Scroll += HandleStartGradientColorChanged;
            generalControlsBox.StartGragientColorBlueTrackBar.Scroll += HandleStartGradientColorChanged;

            generalControlsBox.EndGragientColorRedTrackBar.Scroll += HandleEndGradientColorChanged;
            generalControlsBox.EndGragientColorGreenTrackBar.Scroll += HandleEndGradientColorChanged;
            generalControlsBox.EndGragientColorBlueTrackBar.Scroll += HandleEndGradientColorChanged;

            controls.Controls.Add(generalControlsBox);
        }

        //Обработчик события изменения начальных цветов градиента.
        private void HandleStartGradientColorChanged(object sender, EventArgs e)
        {
            if (sender is TrackBar)
            {
                TrackBar senderBar = (TrackBar)sender;
                int newRedColor = startGradientColor.R;
                int newGreenColor = startGradientColor.G;
                int newBlueColor = startGradientColor.B;
                switch (senderBar.Name)
                {
                    case "StartGragientColorRedTrackBar":
                        newRedColor = senderBar.Value;
                        break;
                    case "StartGragientColorGreenTrackBar":
                        newGreenColor = senderBar.Value;
                        break;
                    case "StartGragientColorBlueTrackBar":
                        newBlueColor = senderBar.Value;
                        break;
                }
                startGradientColor = Color.FromArgb(newRedColor, newGreenColor, newBlueColor);
                //Чтобы при отрисовке фрактала создался новый (измененный) градиент.
                gradient = null;
                RaiseControlsChangedEvent(e);
            }
        }

        //Обработчик события изменения конечных цветов градиента.
        private void HandleEndGradientColorChanged(object sender, EventArgs e)
        {
            if (sender is TrackBar)
            {
                TrackBar senderBar = (TrackBar)sender;
                int newRedColor = endGradientColor.R;
                int newGreenColor = endGradientColor.G;
                int newBlueColor = endGradientColor.B;
                switch (senderBar.Name)
                {
                    case "EndGragientColorRedTrackBar":
                        newRedColor = senderBar.Value;
                        break;
                    case "EndGragientColorGreenTrackBar":
                        newGreenColor = senderBar.Value;
                        break;
                    case "EndGragientColorBlueTrackBar":
                        newBlueColor = senderBar.Value;
                        break;
                }
                endGradientColor = Color.FromArgb(newRedColor, newGreenColor, newBlueColor);
                //Чтобы при отрисовке фрактала создался новый градиент.
                gradient = null;
                RaiseControlsChangedEvent(e);
            }
        }

        //Будет переопределён для каждого фрактала.
        public virtual void Draw(PaintEventArgs fractalCanvas) { }

        //Метод для удобного получения цвета, которым нужно рисовать текущий шаг.
        protected Color GetCurrentColor(int numberIteration)
        {
            if (!useGradient)
            {
                return Color.Black;
            }
            if (gradient == null)
            {
                gradient = new Gradient(startGradientColor, endGradientColor, NumberIterations);
            }
            return gradient.GeColorByIterationNumber(numberIteration);

        }

        //Метод, вызывающий событие, которое говорит главной форме о том,
        //что поменялись "крутилки" и нужно перерисовать фрактал.
        protected void RaiseControlsChangedEvent(EventArgs e)
        {
            ControlsChanged(this, e);
        }

        private void HandleRecursionDepthScrolling(object sender, EventArgs e)
        {
            if (sender is TrackBar)
            {
                NumberIterations = ((TrackBar)sender).Value;
                RaiseControlsChangedEvent(e);
            }
        }

        private void HandleUseGradientCheckBoxChecked(object sender, EventArgs e)
        {
            if (sender is CheckBox)
            {
                useGradient = ((CheckBox)sender).Checked;
                RaiseControlsChangedEvent(e);
            }
        }    


        //Движение фрактала по форме клавишами WASD.
        internal void MoveLeft()
        {
            horizontalOffset -= offsetStep;
        }

        internal void MoveRight()
        {
            horizontalOffset += offsetStep;
        }

        internal void MoveUp()
        {
            verticalOffset -= offsetStep;
        }

        internal void MoveDown()
        {
            verticalOffset += offsetStep;
        }

        internal void NextZoom()
        {
            currentZoomIndex = (currentZoomIndex + 1) % zoomVariants.Length;
            MaxNumberIterations++;
            //Если пользователь отодвинул картинку в приближении, то при отдалении картинка будет смещена слишком далеко.
            if (currentZoomIndex == 0)
            {
                MaxNumberIterations -= zoomVariants.Length;
                recursionDepth = Math.Min(MaxNumberIterations, recursionDepth);
                verticalOffset = 0;
                horizontalOffset = 0;
            }
        }
        //------------------------------------


        //Свойство, отвечающее за длину первой линии фрактала.
        //Когда изменяется размер окна, в Form меняется firstLineCoef.
        //При вызове Draw, это свойство будет возвращать длину первой линии в зависимости от коэфициента и масштаба.
        //Устанавливается значение только 1 раз - при создании фрактала (для каждого фрактала разные длины).
        protected int FirstLineLength
        {
            get { return (int)(firstLineCoef * firstLineLength * zoomVariants[currentZoomIndex]); }
            set 
            {
                firstLineLength = value;
            }
        }
        
        //Максимальная глубина фрактала.
        protected int MaxNumberIterations
        {
            get { return maxRecursionDepth; }
            set
            {
                maxRecursionDepth = value;
                //Меняем максимум на трекБаре.
                generalControlsBox.RecursionDepthTrackBar.Maximum = value;
            }
        }

        //Текущая глубина.
        protected int NumberIterations
        {
            get { return recursionDepth; }
            set
            {
                recursionDepth = value;
                generalControlsBox.RecursionDepthTrackBar.Value = value;
                gradient = null;
            }
        }
    }
}
