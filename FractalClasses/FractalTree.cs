﻿using PRFractals;
using PRFractals.FractalClasses;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PRFractals
{
    class FractalTree: Fractal
    {
        int firstSegmentRotation = 45;
        int secondSegmentRotation = 45;
        //Отношение длины отрезка на глубине n к длине отрезка на глубине n+1;
        double ratio = 1.5;

        FractalTreeControls controlsBox;

        public FractalTree(TableLayoutPanel controlPanel)
            :base(controlPanel)
        {
            MaxNumberIterations = 12;
            NumberIterations = 5;
            FirstLineLength = 100;
            //"Крутилки" для этого дерева.
            this.controlsBox = new FractalTreeControls();
            //Добавляем обработчики событий.
            controlsBox.LeftSegmentAngleTrackBar.Scroll += HandleRightSegmentAngleScrolling;
            controlsBox.RightSegmentAngleTrackBar.Scroll += HandleLeftSegmentAngleScrolling;
            controlsBox.RatioTrackBar.Scroll += HandleRatioScrolling;
            //Добавляем UserControl "крутилок" на форму.
            controlPanel.Controls.Add(controlsBox);
        }

        //Метод для отрисовки "нулевого" шага рекурсии. Вызывется из главной формы при необходимости отрисовки.
        //Из него запустится рекурсивный алгоритм отрисовки фрактала.
        public override void Draw(PaintEventArgs fractalCanvas)
        {
            Graphics canvas = fractalCanvas.Graphics;
            canvas.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            canvas.Clear(Color.White);

            pen = new Pen(Color.Black, 2);
            pen.Color = GetCurrentColor(0);

            var pointBegin = new Point(fractalCanvas.ClipRectangle.Width / 2, fractalCanvas.ClipRectangle.Height -5);
            var pointEnd = new Point(fractalCanvas.ClipRectangle.Width / 2, fractalCanvas.ClipRectangle.Height - FirstLineLength);
            pointBegin.Offset(horizontalOffset, verticalOffset);
            pointEnd.Offset(horizontalOffset, verticalOffset);

            canvas.DrawLine(pen, pointBegin, pointEnd);
            Draw(1, FirstLineLength, pointEnd, firstSegmentRotation,
                 secondSegmentRotation, canvas);
        }
        
        //Метод отрисовки фрактала, будет вызываться рекурсивно.
        public void Draw(int numberIteration, int lineLength, Point pointBegin,
                         int firstAngle, int secondAngle, Graphics canvas)
        { 
            if (numberIteration > NumberIterations) return;

            pen.Color = GetCurrentColor(numberIteration);
            lineLength = (int)(lineLength / ratio);

            var pointEnd1 = new Point(pointBegin.X - (int)(lineLength * Math.Sin(Math.PI * firstAngle / 180.0)),
                                      pointBegin.Y - (int)(lineLength * Math.Cos(Math.PI * firstAngle / 180.0)));
            var pointEnd2 = new Point(pointBegin.X + (int)(lineLength * Math.Sin(Math.PI * secondAngle / 180.0)),
                                      pointBegin.Y - (int)(lineLength * Math.Cos(Math.PI * secondAngle / 180.0)));
           
            canvas.DrawLine(pen, pointBegin, pointEnd1);
            canvas.DrawLine(pen, pointBegin, pointEnd2);
            Draw(numberIteration + 1, lineLength, pointEnd1,
                firstAngle + firstSegmentRotation, secondAngle - secondSegmentRotation, canvas);
            Draw(numberIteration + 1, lineLength, pointEnd2,
                firstAngle - firstSegmentRotation, secondAngle + secondSegmentRotation, canvas);
        }


        //Обработчики событий изменений парамаетров дерева.
        //------------------------------------------------------------------------
        private void HandleRatioScrolling(object sender, EventArgs e)
        {
            if (sender is TrackBar)
            {
                ratio = ((TrackBar)sender).Value / 10.0;
                RaiseControlsChangedEvent(e);
            }
        }

        private void HandleLeftSegmentAngleScrolling(object sender, EventArgs e)
        {
            if (sender is TrackBar)
            {
                firstSegmentRotation = ((TrackBar)sender).Value;
                RaiseControlsChangedEvent(e);
            }
        }

        private void HandleRightSegmentAngleScrolling(object sender, EventArgs e)
        {
            if (sender is TrackBar)
            {
                secondSegmentRotation = ((TrackBar)sender).Value;
                RaiseControlsChangedEvent(e);
            }
        }
        //--------------------------------------------------------------------------
    }
}
