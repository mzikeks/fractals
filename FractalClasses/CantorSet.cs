﻿using PRFractals.FractalControls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PRFractals
{
    class CantorSet: Fractal
    {
        int spacingBetweenSegments = 40;
        CantorSetControls controlsBox;

        public CantorSet(TableLayoutPanel controlPanel)
           : base(controlPanel)
        {
            NumberIterations = 3;
            MaxNumberIterations = 5;
            penThickness = 15;
            FirstLineLength = 600;

            //Создаём специальные крутилки для этого фрактала.
            this.controlsBox = new CantorSetControls();
            controlsBox.DistanceTrackBar.Scroll += HandleDistanceScrolling;
            controlPanel.Controls.Add(controlsBox);
        }

        //Метод для отрисовки "нулевого" шага рекурсии. Вызывется из главной формы при необходимости отрисовки.
        //Из него запустится рекурсивный алгоритм отрисовки фрактала.
        public override void Draw(PaintEventArgs fractalCanvas)
        {
            Graphics canvas = fractalCanvas.Graphics;
            canvas.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            canvas.Clear(Color.White);

            pen = new Pen(Color.Black, penThickness);
            pen.Color = GetCurrentColor(0);

            var pointBegin = new Point(fractalCanvas.ClipRectangle.Width / 2 - FirstLineLength / 2, 15) ;
            var pointEnd = new Point(fractalCanvas.ClipRectangle.Width / 2 + FirstLineLength / 2, 15);
            pointBegin.Offset(horizontalOffset, verticalOffset);
            pointEnd.Offset(horizontalOffset, verticalOffset);

            canvas.DrawLine(pen, pointBegin, pointEnd);
            Draw(1, FirstLineLength, pointBegin, canvas);
        }
       
        //Рекурсивный метод отрисовки фрактала.
        public void Draw(int numberIteration, int lineLength, Point pointBegin, Graphics canvas)
        {
            if (numberIteration > NumberIterations) return;

            pen.Color = GetCurrentColor(numberIteration);
            lineLength = (int)(lineLength / 3);

            var pointBegin1 = new Point(pointBegin.X, pointBegin.Y + spacingBetweenSegments);
            var pointBegin2 = new Point(pointBegin.X + lineLength * 2, pointBegin.Y + spacingBetweenSegments);

            var pointEnd1 = new Point(pointBegin1.X + lineLength, pointBegin1.Y);
            var pointEnd2 = new Point(pointBegin2.X + lineLength, pointBegin2.Y);

            canvas.DrawLine(pen, pointBegin1, pointEnd1);
            canvas.DrawLine(pen, pointBegin2, pointEnd2);
            Draw(numberIteration + 1, lineLength, pointBegin1, canvas);
            Draw(numberIteration + 1, lineLength, pointBegin2, canvas);
        }

        //Обработчик события изменения расстояния между шагами рекурсии. 
        private void HandleDistanceScrolling(object sender, EventArgs e)
        {
            if (sender is TrackBar)
            {
                spacingBetweenSegments = ((TrackBar)sender).Value;
                RaiseControlsChangedEvent(e);
            }
        }
    }
}
