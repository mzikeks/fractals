﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PRFractals
{
    class KochCurve: Fractal
    {
        //Ластик.
        Pen penWhite;

        public KochCurve(TableLayoutPanel controlPanel)
            : base(controlPanel)
        {
            NumberIterations = 3;
            MaxNumberIterations = 5;
            FirstLineLength = 600;
        }

        //Метод для отрисовки "нулевого" шага рекурсии. Вызывется из главной формы при необходимости отрисовки.
        //Из него запустится рекурсивный алгоритм отрисовки фрактала.
        public override void Draw(PaintEventArgs fractalCanvas)
        {
            Graphics canvas = fractalCanvas.Graphics;
            canvas.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            canvas.Clear(Color.White);

            pen = new Pen(Color.Black, penThickness);
            pen.Color = GetCurrentColor(0);

            //Линяя ластика должна быть потолще, иначе остаются следы (из-за сглаживания).
            penWhite = new Pen(Color.White, penThickness + 3);

            var pointA = new Point(fractalCanvas.ClipRectangle.Width / 2 - FirstLineLength / 2, 
                                   fractalCanvas.ClipRectangle.Height / 2 + FirstLineLength / 8);
            var pointB = new Point(fractalCanvas.ClipRectangle.Width / 2 + FirstLineLength / 2,
                                   fractalCanvas.ClipRectangle.Height / 2 + FirstLineLength / 8);
            pointA.Offset(horizontalOffset, verticalOffset);
            pointB.Offset(horizontalOffset, verticalOffset);
            canvas.DrawLine(pen, pointA, pointB);

            Draw(1, FirstLineLength, pointA, pointB, canvas);
        }

        //Рекурсивный метод отрисовки фрактала.
        public void Draw(int numberIteration, int lineLength, Point pointA, Point pointC, Graphics canvas)
        {
            if (numberIteration > NumberIterations) return;

            lineLength = (int)(lineLength / 3);

            //Сначала запустим рисование для отрезков слева и справа от того места,
            //Где мы должны нарисовать треугольник сейчас, то есть на 1 и 3 третях от пришедшего отрезка.
            var pointFirstThridEnd = new Point(pointA.X + (pointC.X - pointA.X) / 3,
                                               pointA.Y + (pointC.Y - pointA.Y) / 3);
            var pointThirdThridBegin = new Point(pointA.X + 2 * (pointC.X - pointA.X) / 3,
                                                 pointA.Y + 2 * (pointC.Y - pointA.Y) / 3);

            Draw(numberIteration + 1, lineLength, pointA, pointFirstThridEnd, canvas); //Первая треть.
            Draw(numberIteration + 1, lineLength, pointThirdThridBegin, pointC, canvas); //Третья треть.

            //Теперь точка А - начало средней трети, а С - конец (отрезок AC - середина).
            pointA = pointFirstThridEnd;
            pointC = pointThirdThridBegin;
            //Теперь вместо середины текущего отрезка нужно нарисовать "выпуклость".
            canvas.DrawLine(penWhite, pointA, pointC);

            int deltaX = pointC.X - pointA.X;
            int deltaY = pointC.Y - pointA.Y;
            double hypotenuse = Math.Sqrt(deltaX * deltaX + deltaY * deltaY);

            //Если вдруг отрезки стали слишком маленькими.
            if (hypotenuse == 0)
            {
                return;
            }

            double sinA = deltaY / hypotenuse;
            double cosA = deltaX / hypotenuse;

            var pointB = new Point((pointA.X + pointC.X) / 2 + (int)(sinA * lineLength * Math.Sqrt(3) / 2),
                                    (pointA.Y + pointC.Y) / 2 - (int)(cosA * lineLength * Math.Sqrt(3) / 2));

            pen.Color = GetCurrentColor(numberIteration);
            canvas.DrawLine(pen, pointA, pointB);
            canvas.DrawLine(pen, pointC, pointB);
            Draw(numberIteration + 1, lineLength, pointA, pointB, canvas);
            Draw(numberIteration + 1, lineLength, pointB, pointC, canvas);
        }
    }
}
