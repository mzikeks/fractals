﻿namespace PRFractals.FractalControls
{
    partial class CantorSetControls
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DistanceTrackBar = new System.Windows.Forms.TrackBar();
            this.DistanceLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.DistanceTrackBar)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // DistanceTrackBar
            // 
            this.DistanceTrackBar.LargeChange = 15;
            this.DistanceTrackBar.Location = new System.Drawing.Point(3, 28);
            this.DistanceTrackBar.Maximum = 100;
            this.DistanceTrackBar.Name = "DistanceTrackBar";
            this.DistanceTrackBar.Size = new System.Drawing.Size(185, 56);
            this.DistanceTrackBar.SmallChange = 5;
            this.DistanceTrackBar.TabIndex = 15;
            this.DistanceTrackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.DistanceTrackBar.Value = 25;
            // 
            // DistanceLabel
            // 
            this.DistanceLabel.AutoSize = true;
            this.DistanceLabel.Location = new System.Drawing.Point(3, 0);
            this.DistanceLabel.Name = "DistanceLabel";
            this.DistanceLabel.Size = new System.Drawing.Size(84, 25);
            this.DistanceLabel.TabIndex = 2;
            this.DistanceLabel.Text = "Distance";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.DistanceTrackBar, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.DistanceLabel, 0, 2);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(195, 88);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // CantorSetControls
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "CantorSetControls";
            this.Size = new System.Drawing.Size(203, 98);
            ((System.ComponentModel.ISupportInitialize)(this.DistanceTrackBar)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label DistanceLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        internal System.Windows.Forms.TrackBar DistanceTrackBar;
    }
}
