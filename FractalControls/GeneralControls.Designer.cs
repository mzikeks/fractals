﻿namespace PRFractals
{
    partial class GeneralControls
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DepthLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.RecursionDepthTrackBar = new System.Windows.Forms.TrackBar();
            this.UseGradientCheckBox = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.EndGragientColorRedTrackBar = new System.Windows.Forms.TrackBar();
            this.StartGragientColorRedTrackBar = new System.Windows.Forms.TrackBar();
            this.StartGragientColorBlueTrackBar = new System.Windows.Forms.TrackBar();
            this.StartGragientColorGreenTrackBar = new System.Windows.Forms.TrackBar();
            this.EndGragientColorGreenTrackBar = new System.Windows.Forms.TrackBar();
            this.EndGragientColorBlueTrackBar = new System.Windows.Forms.TrackBar();
            this.SaveButton = new System.Windows.Forms.Button();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RecursionDepthTrackBar)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EndGragientColorRedTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartGragientColorRedTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartGragientColorBlueTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartGragientColorGreenTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndGragientColorGreenTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndGragientColorBlueTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            this.SuspendLayout();
            // 
            // DepthLabel
            // 
            this.DepthLabel.AutoSize = true;
            this.DepthLabel.Location = new System.Drawing.Point(3, 0);
            this.DepthLabel.Name = "DepthLabel";
            this.DepthLabel.Size = new System.Drawing.Size(148, 25);
            this.DepthLabel.TabIndex = 1;
            this.DepthLabel.Text = "Recursion depth";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.RecursionDepthTrackBar, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.DepthLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.UseGradientCheckBox, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.SaveButton, 0, 4);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(195, 404);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // RecursionDepthTrackBar
            // 
            this.RecursionDepthTrackBar.Location = new System.Drawing.Point(3, 28);
            this.RecursionDepthTrackBar.Maximum = 15;
            this.RecursionDepthTrackBar.Name = "RecursionDepthTrackBar";
            this.RecursionDepthTrackBar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RecursionDepthTrackBar.Size = new System.Drawing.Size(185, 56);
            this.RecursionDepthTrackBar.TabIndex = 0;
            this.RecursionDepthTrackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.RecursionDepthTrackBar.Value = 7;
            // 
            // UseGradientCheckBox
            // 
            this.UseGradientCheckBox.AutoSize = true;
            this.UseGradientCheckBox.Location = new System.Drawing.Point(10, 90);
            this.UseGradientCheckBox.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.UseGradientCheckBox.Name = "UseGradientCheckBox";
            this.UseGradientCheckBox.Size = new System.Drawing.Size(107, 29);
            this.UseGradientCheckBox.TabIndex = 3;
            this.UseGradientCheckBox.Text = "Gradient";
            this.UseGradientCheckBox.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.EndGragientColorRedTrackBar, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.StartGragientColorRedTrackBar, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.StartGragientColorBlueTrackBar, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.StartGragientColorGreenTrackBar, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.EndGragientColorGreenTrackBar, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.EndGragientColorBlueTrackBar, 2, 2);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 125);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(189, 207);
            this.tableLayoutPanel2.TabIndex = 4;
            // 
            // EndGragientColorRedTrackBar
            // 
            this.EndGragientColorRedTrackBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.EndGragientColorRedTrackBar.Location = new System.Drawing.Point(3, 109);
            this.EndGragientColorRedTrackBar.Maximum = 255;
            this.EndGragientColorRedTrackBar.Name = "EndGragientColorRedTrackBar";
            this.EndGragientColorRedTrackBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EndGragientColorRedTrackBar.Size = new System.Drawing.Size(56, 95);
            this.EndGragientColorRedTrackBar.SmallChange = 5;
            this.EndGragientColorRedTrackBar.TabIndex = 1;
            this.EndGragientColorRedTrackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
            // 
            // StartGragientColorRedTrackBar
            // 
            this.StartGragientColorRedTrackBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.StartGragientColorRedTrackBar.Location = new System.Drawing.Point(3, 3);
            this.StartGragientColorRedTrackBar.Maximum = 255;
            this.StartGragientColorRedTrackBar.Name = "StartGragientColorRedTrackBar";
            this.StartGragientColorRedTrackBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.StartGragientColorRedTrackBar.Size = new System.Drawing.Size(56, 100);
            this.StartGragientColorRedTrackBar.SmallChange = 5;
            this.StartGragientColorRedTrackBar.TabIndex = 1;
            this.StartGragientColorRedTrackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
            // 
            // StartGragientColorBlueTrackBar
            // 
            this.StartGragientColorBlueTrackBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.StartGragientColorBlueTrackBar.Location = new System.Drawing.Point(127, 3);
            this.StartGragientColorBlueTrackBar.Maximum = 255;
            this.StartGragientColorBlueTrackBar.Name = "StartGragientColorBlueTrackBar";
            this.StartGragientColorBlueTrackBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.StartGragientColorBlueTrackBar.Size = new System.Drawing.Size(56, 100);
            this.StartGragientColorBlueTrackBar.TabIndex = 1;
            this.StartGragientColorBlueTrackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
            // 
            // StartGragientColorGreenTrackBar
            // 
            this.StartGragientColorGreenTrackBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StartGragientColorGreenTrackBar.Location = new System.Drawing.Point(65, 3);
            this.StartGragientColorGreenTrackBar.Maximum = 255;
            this.StartGragientColorGreenTrackBar.Name = "StartGragientColorGreenTrackBar";
            this.StartGragientColorGreenTrackBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.StartGragientColorGreenTrackBar.Size = new System.Drawing.Size(56, 100);
            this.StartGragientColorGreenTrackBar.TabIndex = 1;
            this.StartGragientColorGreenTrackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
            // 
            // EndGragientColorGreenTrackBar
            // 
            this.EndGragientColorGreenTrackBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.EndGragientColorGreenTrackBar.Location = new System.Drawing.Point(65, 109);
            this.EndGragientColorGreenTrackBar.Maximum = 255;
            this.EndGragientColorGreenTrackBar.Name = "EndGragientColorGreenTrackBar";
            this.EndGragientColorGreenTrackBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EndGragientColorGreenTrackBar.Size = new System.Drawing.Size(56, 95);
            this.EndGragientColorGreenTrackBar.SmallChange = 5;
            this.EndGragientColorGreenTrackBar.TabIndex = 1;
            this.EndGragientColorGreenTrackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
            // 
            // EndGragientColorBlueTrackBar
            // 
            this.EndGragientColorBlueTrackBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.EndGragientColorBlueTrackBar.Location = new System.Drawing.Point(127, 109);
            this.EndGragientColorBlueTrackBar.Maximum = 255;
            this.EndGragientColorBlueTrackBar.Name = "EndGragientColorBlueTrackBar";
            this.EndGragientColorBlueTrackBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.EndGragientColorBlueTrackBar.Size = new System.Drawing.Size(56, 95);
            this.EndGragientColorBlueTrackBar.SmallChange = 5;
            this.EndGragientColorBlueTrackBar.TabIndex = 1;
            this.EndGragientColorBlueTrackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(3, 338);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(189, 33);
            this.SaveButton.TabIndex = 5;
            this.SaveButton.Text = "Сохранить как...";
            this.SaveButton.UseVisualStyleBackColor = true;
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(3, 3);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBar1.Size = new System.Drawing.Size(56, 100);
            this.trackBar1.TabIndex = 1;
            this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.Both;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(3, 106);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "Red";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(65, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "Green";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(119, 106);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "Blue";
            // 
            // trackBar2
            // 
            this.trackBar2.Location = new System.Drawing.Point(0, 0);
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Size = new System.Drawing.Size(104, 56);
            this.trackBar2.TabIndex = 0;
            // 
            // GeneralControls
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "GeneralControls";
            this.Size = new System.Drawing.Size(194, 383);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RecursionDepthTrackBar)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EndGragientColorRedTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartGragientColorRedTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartGragientColorBlueTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartGragientColorGreenTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndGragientColorGreenTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndGragientColorBlueTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label DepthLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        internal System.Windows.Forms.TrackBar RecursionDepthTrackBar;
        internal System.Windows.Forms.CheckBox UseGradientCheckBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        internal System.Windows.Forms.TrackBar StartGragientColorRedTrackBar;
        internal System.Windows.Forms.TrackBar StartGragientColorBlueTrackBar;
        internal System.Windows.Forms.TrackBar StartGragientColorGreenTrackBar;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TrackBar trackBar2;
        internal System.Windows.Forms.TrackBar EndGragientColorRedTrackBar;
        internal System.Windows.Forms.TrackBar EndGragientColorGreenTrackBar;
        internal System.Windows.Forms.TrackBar EndGragientColorBlueTrackBar;
        internal System.Windows.Forms.Button SaveButton;
    }
}
