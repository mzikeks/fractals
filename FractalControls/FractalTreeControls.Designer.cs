﻿namespace PRFractals
{
    partial class FractalTreeControls
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.RightSegmentAngleTrackBar = new System.Windows.Forms.TrackBar();
            this.LeftSegmentAngleTrackBar = new System.Windows.Forms.TrackBar();
            this.LeftSegmentAngleLabel = new System.Windows.Forms.Label();
            this.RatioLabel = new System.Windows.Forms.Label();
            this.RatioTrackBar = new System.Windows.Forms.TrackBar();
            this.RightSegmentAngleLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RightSegmentAngleTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftSegmentAngleTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RatioTrackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.RightSegmentAngleTrackBar, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.LeftSegmentAngleTrackBar, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.LeftSegmentAngleLabel, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.RatioLabel, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.RatioTrackBar, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.RightSegmentAngleLabel, 0, 4);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(182, 245);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // RightSegmentAngleTrackBar
            // 
            this.RightSegmentAngleTrackBar.LargeChange = 15;
            this.RightSegmentAngleTrackBar.Location = new System.Drawing.Point(3, 111);
            this.RightSegmentAngleTrackBar.Maximum = 135;
            this.RightSegmentAngleTrackBar.Minimum = 15;
            this.RightSegmentAngleTrackBar.Name = "RightSegmentAngleTrackBar";
            this.RightSegmentAngleTrackBar.Size = new System.Drawing.Size(191, 56);
            this.RightSegmentAngleTrackBar.SmallChange = 5;
            this.RightSegmentAngleTrackBar.TabIndex = 3;
            this.RightSegmentAngleTrackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.RightSegmentAngleTrackBar.Value = 45;
            // 
            // LeftSegmentAngleTrackBar
            // 
            this.LeftSegmentAngleTrackBar.LargeChange = 15;
            this.LeftSegmentAngleTrackBar.Location = new System.Drawing.Point(3, 26);
            this.LeftSegmentAngleTrackBar.Maximum = 135;
            this.LeftSegmentAngleTrackBar.Minimum = 15;
            this.LeftSegmentAngleTrackBar.Name = "LeftSegmentAngleTrackBar";
            this.LeftSegmentAngleTrackBar.Size = new System.Drawing.Size(191, 56);
            this.LeftSegmentAngleTrackBar.SmallChange = 5;
            this.LeftSegmentAngleTrackBar.TabIndex = 15;
            this.LeftSegmentAngleTrackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.LeftSegmentAngleTrackBar.Value = 45;
            // 
            // LeftSegmentAngleLabel
            // 
            this.LeftSegmentAngleLabel.AutoSize = true;
            this.LeftSegmentAngleLabel.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.LeftSegmentAngleLabel.Location = new System.Drawing.Point(3, 0);
            this.LeftSegmentAngleLabel.Name = "LeftSegmentAngleLabel";
            this.LeftSegmentAngleLabel.Size = new System.Drawing.Size(156, 23);
            this.LeftSegmentAngleLabel.TabIndex = 2;
            this.LeftSegmentAngleLabel.Text = "Left segment angle";
            // 
            // RatioLabel
            // 
            this.RatioLabel.AutoSize = true;
            this.RatioLabel.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.RatioLabel.Location = new System.Drawing.Point(3, 170);
            this.RatioLabel.Name = "RatioLabel";
            this.RatioLabel.Size = new System.Drawing.Size(49, 23);
            this.RatioLabel.TabIndex = 5;
            this.RatioLabel.Text = "Ratio";
            // 
            // RatioTrackBar
            // 
            this.RatioTrackBar.Location = new System.Drawing.Point(3, 196);
            this.RatioTrackBar.Maximum = 20;
            this.RatioTrackBar.Minimum = 11;
            this.RatioTrackBar.Name = "RatioTrackBar";
            this.RatioTrackBar.Size = new System.Drawing.Size(191, 56);
            this.RatioTrackBar.TabIndex = 6;
            this.RatioTrackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.RatioTrackBar.Value = 15;
            // 
            // RightSegmentAngleLabel
            // 
            this.RightSegmentAngleLabel.AutoSize = true;
            this.RightSegmentAngleLabel.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.RightSegmentAngleLabel.Location = new System.Drawing.Point(3, 85);
            this.RightSegmentAngleLabel.Name = "RightSegmentAngleLabel";
            this.RightSegmentAngleLabel.Size = new System.Drawing.Size(168, 23);
            this.RightSegmentAngleLabel.TabIndex = 4;
            this.RightSegmentAngleLabel.Text = "Right segment angle";
            this.RightSegmentAngleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FractalTreeControls
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "FractalTreeControls";
            this.Size = new System.Drawing.Size(194, 257);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RightSegmentAngleTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftSegmentAngleTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RatioTrackBar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label LeftSegmentAngleLabel;
        private System.Windows.Forms.Label RightSegmentAngleLabel;
        private System.Windows.Forms.Label RatioLabel;
        internal System.Windows.Forms.TrackBar LeftSegmentAngleTrackBar;
        internal System.Windows.Forms.TrackBar RightSegmentAngleTrackBar;
        internal System.Windows.Forms.TrackBar RatioTrackBar;
    }
}
