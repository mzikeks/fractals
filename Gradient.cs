﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PRFractals.FractalClasses
{
    //Класс для получения цвета градиента на каждом шаге постоения фракталов.
    class Gradient
    {
        private int stepColorRed;
        private int stepColorGreen;
        private int stepColorBlue;

        Color startColor;

        public Gradient(Color startColor, Color endColor, int nSteps)
        {
            this.startColor = startColor;
            if (nSteps < 1)
            {
                return;
            }
            stepColorRed = (endColor.R - startColor.R) / nSteps;
            stepColorGreen = (endColor.G - startColor.G) / nSteps;
            stepColorBlue = (endColor.B - startColor.B) / nSteps;
        }

        public Color GeColorByIterationNumber(int iterationNumber)
        {
            return Color.FromArgb(startColor.R + stepColorRed * iterationNumber,
                                  startColor.G + stepColorGreen * iterationNumber,
                                  startColor.B + stepColorBlue * iterationNumber); 
        }
    }
}
